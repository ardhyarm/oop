<?php
//require('animal.php');
require('Frog.php');
require('Ape.php');


$sheep = new Animal("Shaun");

echo "Animal name : $sheep->name <br>"; // "shaun"
echo "Legs :$sheep->legs <br>"; // 2
echo "Cold blooded :$sheep->cold_blooded <br><br>"; // false

$kodok = new Frog("buduk");

echo "Animal name : $kodok->name <br>";
echo "Legs : $kodok->legs <br>";
echo "Cold blooded : $kodok->cold_blooded <br> ";
$kodok->jump(); // "hop hop"

$sungokong = new Ape("kera sakti");

echo "Animal name : $sungokong->name <br>";
echo "Legs : $sungokong->legs <br>";
echo "Cold blooded : $sungokong->cold_blooded<br>";
$sungokong->yell() // "Auooo"





?>